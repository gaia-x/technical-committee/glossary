# Shapes Constraint Language (SHACL)

Language for validating RDF graphs against a set of conditions. These conditions are provided as shapes and other constructs expressed in the form of an RDF graph. 

## References
[SHACL specifications](https://www.w3.org/TR/shacl/).

## See Also
- [RDF](rdf.md)
- [Shape](shape.md)