# Gaia-X Data Exchange Document

The Gaia-X Data Exchange document, also known as the Data Exchange Services specifications is a Gaia-X deliverable that provides specifications for Data Exchange Services, including high level architecture and key requirements for data value, trust and compliance.

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- [Gaia-X Data Exchange Document](https://docs.gaia-x.eu/technical-committee/data-exchange/23.11/)

## See Also
- [Gaia-X deliverable](gaia-x_deliverables.md)
- [Data Exchange Services](data_exchange_services.md)
