# Gaia-X Open Source Software Community

The Gaia-X Open-Source Software (OSS) Community aims to engage and grow the Gaia-X developer community through outreach, onboarding, software development and the organization of the Gaia-X hackathon event.

## References
[Gaia-X Open-Source Software Community - Gitlab repository](https://gitlab.com/gaia-x/gaia-x-community/open-source-community) 

