# Keypair

A private key and its corresponding public key; a key pair is used with an asymmetric-key (public-key) algorithm.

## References
[NIST - Recommendation for Cryptographic Key Generation](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-133r2.pdf)
