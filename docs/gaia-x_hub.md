# Gaia-X Hub

Gaia-X Hubs bundle user interests across Europe to facilitate the creation of European Data Spaces. They gather use cases, requirements, and standards, to support the Gaia-X Association in setting up and establishing a sovereign data-infrastructure via a common Gaia-X architecture as well as policy rules, standards and Federated Services. Gaia-X Hubs are the central contact points for interested parties in each country, and grassroots supporters of the Gaia-X project.

## References
[Gaia-X website](https://gaia-x.eu/community/hubs/)

