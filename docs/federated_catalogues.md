# Federated Catalogues

The goal of the Federated Catalogue is to:
- enable Consumers to find best-matching offerings and to monitor them for relevant changes in the offerings.
- enable Producers to promote their offerings while keeping full control of the level of visibility and privacy of their offerings.
- enable Service Composition by including and publishing Service Descriptions, conformant to the Gaia-X Schema, that contain structured service attributes required to compose services.
- avoid a gravity effect with a lock-out and lock-in effect around a handful of catalogue instances.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/enabling_services/#federated-catalogues)- 7.3

## See Also
[Offerings](service_offering.md)

