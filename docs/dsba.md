# DSBA (Data Spaces Business Alliance)

The Data Spaces Business Alliance (DSBA) accelerates business transformation in the data economy. It’s the first initiative of its kind, uniting industry players to realize a data-driven future in which organizations and individuals can unlock the full value of their data.

The Data Spaces Business Alliance are [Gaia-X European Association for Data and Cloud AISBL](gaia-x.md), the [Big Data Value Association (BDVA)](https://www.bdva.eu/), [FIWARE Foundation](https://www.fiware.org/), and the [International Data Spaces Association (IDSA)](https://internationaldataspaces.org/). 

## References
https://data-spaces-business-alliance.eu/
