# Identifier

Each Gaia-X participant provides a unique identifier that is associated with the attributes and the participant can cryptographically prove that this Identifier is under his control.
The attributes are evaluated by the Participants to implement, enforce, and monitor permissions, prohibitions and duties when negotiating a contract for services or resources.
An identifier is a unique attribute that is part of the Participant’s Identity.

## References
- [Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#identity-and-access-management) 4.4
- [W3C Decentralized Identifiers](https://www.w3.org/TR/did-core/)

## See Also
[Gaia-X Participant](gaia-x_participant.md)
