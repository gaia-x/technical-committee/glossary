# Gaia-X Lighthouse projects

Projects aiming to create a data exchange platform built on transparency, trust, and openness. They target multiple industries and are the front-runners implementing the Gaia-X Framework.

## References
[Gaia-X website](https://gaia-x.eu/community/lighthouse-projects/)
