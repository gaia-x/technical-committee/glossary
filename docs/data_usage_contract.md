# Data Usage Contract 

Before using a data product, the data consumer negotiates and co-signs a Data Usage Contract (DUC) with the Data Provider. This Data Usage Contract is based on the data product description and includes the service configuration element and mutually agreed and enforceable Terms of Usage, resulting from potential negotiations. 

## References
[Gaia-X Architecture Document 25.XX - ongoing version](https://gaia-x.gitlab.io/technical-committee/architecture-working-group/architecture-document/component_details/#data-product-conceptual-model) - 4.5.1 

## See Also
- [Data Product](data_product.md)
- [Data Consumer](data_consumer.md) 
- [Data Product Description](data_product_description.md)
