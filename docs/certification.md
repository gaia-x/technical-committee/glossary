# Certification

Certification is the third-party attestation related to an object of conformity assessment, with the exception of accredition.

## References
[ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5)

## See Also
- [Attestation](attestation.md)
- [Conformity Assessment](conformity_assessment.md)
- [Accreditation](accreditation.md)