# Knowledge graph

The Gaia-X credentials are the building blocks of a decentralized machine-readable knowledge graph of claims, each credential carrying a tamper-proof and authenticable part of the information of that graph.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#gaia-x-credentials)- 3.3.1

## See Also
- [Gaia-X Credentials](gaia-x_credential.md)
- [Claims](claim.md)