# Credential

A set of one or more claims made by the same entity.

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#credentials)

## See Also
- [Claim](claim.md)
- [entity](entity.md)