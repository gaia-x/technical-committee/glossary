# Notary

Notaries are entities accredited by the Gaia-X Association, which perform validation based on objective evidence from a Gaia-X Trusted Data sources, digitalizing an assessment previously made.
The Notaries are converting “not machine readable” proofs into “machine-readable” proofs.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#gaia-x-trusted-data-sources-and-gaia-x-notaries)- 4.6.1

