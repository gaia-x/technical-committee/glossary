# Gaia-X Data & Services Business Committee (DSBC)

The Data & Services Business Committee (DSBC) collects, shares, and aligns needs and achievements between national Hubs, Ecosystems, Gaia-X Lighthouses & projects, and Service Providers to support the creation of Data Spaces and accelerate Gaia-X market adoption. 

## References
[Gaia-X website](https://gaia-x.eu/about/)