# Verifier

A role an entity performs by receiving one or more verifiable credentials, optionally inside a verifiable presentation for processing.

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#dfn-verifier)

## See Also
- [Verifiable Credential](verifiable_credential.md)
- [Verifiable Presentation](verifiable_presentation.md)