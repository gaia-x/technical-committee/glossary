# Permissible Standard

Permissible Standards shall identify standards respectively requirements/controls
within such standards, where implementation shall be considered prima facie evidence of conformity with the related Gaia-X criterion. 

## References
[Gaia-X Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Introduction_and_scope/#mapping-and-referencing-of-existing-standards) - 3.2.5

