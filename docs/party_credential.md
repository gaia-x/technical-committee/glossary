# Party Credential

The Party Credential is based upon the Verifiable Credentials Data Model v2.0 and is the basement for all IAA Parties such as Natural Persons, Services, Legal Persons, etc.

## References
[Identity,Credential and Access Management Document 24.07](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/24.07/TA_credential_and_party_credential/#party-credential)- 5

## See Also
[Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/)