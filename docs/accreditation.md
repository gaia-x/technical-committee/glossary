# Accreditation

Accredition is the third-party attestation related to a conformity assessment body, conveying formal demonstration of its competence, impartiality and consistent operation in performing specific conformity assessment activities.

## References
[ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5)

## See Also
- [Attestation](attestation.md)
- [conformity assessment body](conformity_assessment_body)