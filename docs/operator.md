# Operator

Operators are Gaia-X Providers that have been approved by the ecosystem governance to operate federation services and the Federation, which are independent of each other. There can be one or more Operators per type of Federation Service.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#operator)- 3.2.2.2 

## See Also
[Federation Services](federation_services.md)