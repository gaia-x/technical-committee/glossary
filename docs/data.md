# Data

Data means any digital representation of acts, facts or information and any compilation of such acts, facts or information.

## References
- [Data Exchange Document 23.10](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/dewg/) - 2.2 
- [Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#data-product-conceptual-model) - 4.5.1