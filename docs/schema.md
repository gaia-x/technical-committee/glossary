# Schema

The Gaia-X members define the Schema for Gaia-X credentials. It is used as the vocabulary of the claims about credential subjects and must be available in the form of SHACL shapes (cf. the W3C
Shapes Constraint Language SHACL).
The defined version of the Gaia-X Schema is maintained and made available through the Gaia-X Registry.

## References
- [Gaia-X Architecture Document 23.10](https://docs.gaia-x.eu/technical-committee/architecture-document/23.10/) - 3.3.1.1
- https://www.w3.org/TR/vc-data-model-2.0/#data-schemas

## See Also
- [Gaia-X Credential](gaia-x_credential.md)
- [Claim](claim.md)
- [Gaia-X Registry](gaia-x_registry.md)