# Verifiable Presentation

A verifiable presentation is a tamper-evident presentation encoded in such a way that authorship of the data  can be trusted after a process of cryptographic verification.
Certain types of verifiable presentations might contain data that is synthesized from, but do not contain, the original verifiable credentials (for example, zero-knowledge proofs).

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#dfn-verifiable-presentation)

## See Also
- [Data](data.md)
- [Verifiable Credential](verifiable_credential.md)