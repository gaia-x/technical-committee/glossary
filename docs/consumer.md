# Consumer

Consumer is a participant who searches service offerings and consumes service instances in the Gaia-X Ecosystem to enable digital offerings for end users.


## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#consumer) - 3.2.2.3


## See Also
- [Participant](participant.md)
- [Service Offerings](service_offering.md)
- [Service Instances](service-instance.md)
- [Gaia-X Ecosystem](gaia-x_ecosystem.md)
- [End-Users](end-user.md)
- [Policy](policy.md)
- [Consumer Policy](consumer_policy.md)
