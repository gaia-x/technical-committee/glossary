# Shape 

The Gaia-X members define the Schema for Gaia-X credentials. It is used as the vocabulary of the claims about credential subjects and must be available in the form of SHACL shapes.

## References
- [Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#gaia-x-schema/)- 3.3.1.1
- [W3C Shapes Constraint Language SHACL](https://www.w3.org/TR/shacl/)

## See Also
- [Gaia-X Credentials](gaia-x_credential.md)
- [W3C Shapes Constraint Language SHACL](https://www.w3.org/TR/shacl/)