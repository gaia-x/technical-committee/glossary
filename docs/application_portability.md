# Application portability

Application Portability refers to the porting of customer or third party executable code from one cloud service to another, public or private.

## References
- [Gaia-X Policy Rules Conformity Document 23.10](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/latest/) - 11.5
- [ISO/IEC 19941:2017](https://www.iso.org/standard/66639.html) - 5.2.3
