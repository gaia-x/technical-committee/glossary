# Data Consumer

A participant that receives data in the form of a data product. The data is used for query, analysis, reporting or any other data processing.

## References
- [Data Exchange Document 23.10](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/dewg/) - 2.2 
- [Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#data-product-conceptual-model) 4.5.1

## See Also
[Data Product](data_product.md)
