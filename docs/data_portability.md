# Data Portability

This refers to the porting of data (structured or unstructured) from one cloud service to another, public or private.

## References
[Gaia-X Policy Rules Conformity Document 23.10](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/latest/) 11.5

## See Also
[Data](data.md)