# Metadata 

Data about other data, documents, or set of data that describes their content, context, structure, data format, provenance, and/or rights attached to them.

## References
[Data Exchange Document 23.10](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/dewg/) - 2.2 

## See Also
[Data](data.md)