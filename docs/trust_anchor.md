# Trust Anchor

Gaia-X Trust Anchors are bodies, parties, i.e., Conformity Assessment Bodies or technical means accredited by the Gaia-X Association to be parties eligible to issue attestations about specific claims. For each accredited Trust Anchor, a specific scope of attestation is defined.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Gaia-X_Trust_Anchors/#gaia-x-trust-anchors)- 6

## See Also
- [Accredited](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.7)
- [Scope of attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4)

