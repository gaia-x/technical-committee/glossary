# Federation Services

Federation Services are enabling services operated under the specific rule of ecosystem governance.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/enabling_services/#enabling-and-federation-services)- 7

## See Also
[Enabling services](enabling_services.md)