# Holder

A role an entity might perform by possessing one or more verifiable credentials and generating verifiable presentations from them. 

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#dfn-holders)

## See Also
- [Verifiable Credentials](verifiable_credentials.md)
- [verifiable presentations](verifiable_presentation.md)