# Issuer

A role an entity can perform by asserting claims about one or more subjects, creating a verifiable credential from these claims, and transmitting the verifiable credential to a holder.

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#dfn-issuers)

## See Also
- [Claims](claims.md)
- [Verifiable credential](verifiable_credential.md)
- [Holder](holder.md)
