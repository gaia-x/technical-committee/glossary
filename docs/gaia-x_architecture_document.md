# Gaia-X Architecture Document

The Gaia-X Architecture Documentm also known as Architecture Document is a Gaia-X deliverable that explains the core elements that compose the Gaia-X Trust Framework and defines how they relate to each other at the functional level in the Gaia-X model.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/context/#context) - 2

## See Also
[Gaia-X deliverable](gaia-x_deliverable)
