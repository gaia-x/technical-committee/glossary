# Service Offering

A Service Offering is a set of resources, which a provider aggregates and publishes as a single entry in a catalogue. 

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#resources-and-service-offerings)- 4.1

## See Also
- [Resources](resource.md)
- [Provider](provider.md)
- [Catalogue](catalogue.md)