# Data Provider

Data are furnished by Data Producers (who are either data owners or data controllers in the GDPR sense, or data holders in the Data Act sense) to Data Providers who compose these data into a Data Product to be used by Data Consumers.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/component_details/#data-product-conceptual-model) - 4.5.1

## See Also
- [Data Producer](data_producer.md)
- [Data Consumers](data_consumer.md)