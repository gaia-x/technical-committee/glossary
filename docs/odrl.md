# ODRL (Open Digital Rights Language)

The Open Digital Rights Language (ODRL) is a policy expression language that provides a flexible and interoperable information model, vocabulary, and encoding mechanisms for representing statements about the usage of content and services.

# References
[ODRL Information Model 2.2](https://www.w3.org/TR/odrl-model/)
