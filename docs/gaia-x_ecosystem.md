# Gaia-X Ecosystem

The Gaia-X Ecosystem is the virtual set of participants and Service Offerings following the Gaia-X Compliance requirements.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Executive_Summary/#executive-summary)

## See Also
[Participants](participant.md)
