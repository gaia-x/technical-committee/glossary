# Trust Indexes

Scoring tools meant to be used by all parties in ecosystems as a measure of distance for interoperability and trust with regards to the other offerings in the ecosystem catalogues. 

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/annex_trust_indexes/#trust-indexes)- 12
