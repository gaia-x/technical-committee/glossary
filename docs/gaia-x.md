# Gaia-X

The Gaia-X European Association for Data and Cloud (AISBL) also known as Gaia-X Association was established as a private not-for-profit Association in 2021. Gaia-X brings together a broad range of organisations (large companies and SMEs, developers and users of technology, industrial players, and members of academia) around one common goal: to boost the European data economy by enabling the creation of common data spaces, in full alignment with the objectives of the EU’s Data Strategy. To this end, Gaia-X focuses on building a common standard for an open, transparent, and secure digital ecosystem that will serve as the basis for a new model of data infrastructure guaranteeing safe and trustworthy data exchange.

## References
<http://www.ejustice.just.fgov.be/tsv_pdf/2021/02/08/21017239.pdf>



