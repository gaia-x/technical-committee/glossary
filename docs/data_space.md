# Data Space

Interoperable framework, based on common governance principles, standards, practices and enabling services, that enables trusted data transactions between participants.

## References
[DSSC Glossary, update October 2024](https://dssc.eu/space/bv15e/766061638/1+Key+Concept+Definitions)

