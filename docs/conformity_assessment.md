# Conformity Assessment

Conformity Assessment is a demonstration that specified requirements (need or expectation that is stated) are fulfilled.



## References
[ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5)

## See Also
[Conformity Assessment Body](conformity_assessment_body.md)