# Gaia-X Identity, Credential and Access Management (ICAM) Document

The Gaia-X Identity, Credential and Access Management (ICAM) document, also known as ICAM specifications is a Gaia-X deliverable aimed at describing the components for “Authorization & Authentication” which shall deliver core functionalities for authorization, access management and authentication as well as services around it to Gaia-X participants with the purpose to join the trustful environment of the ecosystem.

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- [Gaia-X Identity, Credentials and Access Management Document](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/24.07/)

## See Also
- [Gaia-X deliverable](gaia-x_deliverables.md)
- [Participants](participant.md)
- [Ecosystem](gaia-x_ecosystem.md)
