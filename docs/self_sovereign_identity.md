# Self-Sovereign Identity (SSI)

SSI allow users to control and own their digital identities and other verifiable digital credentials locally. It is not required to use a 
predominant cloud service provider, nor is the establishment of a central Gaia-X Identity provider necessary. Users are thus 
completely independent of third parties and decide themselves which identity data they share with whom, as all identity data 
is securely stored only with the individual user in their SSI wallet. 

## References
- [Gaia-X secure and trustworthy ecosystems with Self Sovereign Identity - Whitepaper](https://gaia-x.eu/wp-content/uploads/2022/06/SSI_White_Paper_Design_Final_EN.pdf)
- https://en.wikipedia.org/wiki/Self-sovereign_identity 
