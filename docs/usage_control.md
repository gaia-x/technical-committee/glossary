# Usage Control

Usage control is an extension to traditional access control. It is about the specification and enforcement of restrictions regulating what must (not) happen to data.

## References
[Data Exchange Document 23.11](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/) - 2.6

## See Also
- [Access control](access_control.md)
- [Data](data.md)