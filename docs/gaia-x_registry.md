# Gaia-X Registry

The [Gaia-X Registry](https://registry.gaia-x.eu) is a public distributed, non-repudiable, immutable, permissionless database with a decentralised infrastructure and the capacity to automate code execution.
The Gaia-X Registry is the backbone of the ecosystem governance, which stores information such as:
- the list of the Trust Anchors – keyring.
- the result of the Trust Anchors validation processes.
- the potential revocation of Trust Anchors’s identity.
- the vote and results of the Gaia-X Association roll call vote.
- the shapes and schemas for the Gaia-X VCs.
- the URLs of Gaia-X Catalogue’s credentials.
- the text of the Terms and Conditions for Gaia-X Conformity.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-registry)- 6.1