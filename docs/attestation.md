# Attestation

Issue of a statement, based on a decision, that fulfilment of specified requirements has been demonstrated.

Note 1 to entry: The resulting statement, referred to in this document as a “statement of conformity”, is intended to convey the assurance that the specified requirements have been fulfilled. Such an assurance does not, of itself, provide contractual or other legal guarantees.

Note 2 to entry: First-party attestation and third-party attestation are distinguished by the terms declaration, certification and accreditation, but there is no corresponding term applicable to second-party attestation.

## References
[ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3)
