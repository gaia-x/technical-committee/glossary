# Gaia-X Framework

The Gaia-X Framework provides an overall view of the Gaia-X Association pillars and deliverables, highlighting the elements that are mandatory to be Gaia-X Compliant and Gaia-X Technical Compatible.

## References
[Gaia-X Framework](https://docs.gaia-x.eu/framework/)

## See Also
[Deliverables](gaia-x_deliverables.md)