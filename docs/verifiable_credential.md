# Verifiable Credential

A verifiable credential is a tamper-evident credential that has authorship that can be cryptographically verified. Verifiable credentials can be used to build verifiable presentations, which can also be cryptographically verified.

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#dfn-verifiable-credential)

## See Also
- [Credential](credential.md)
- [Verifiable Presentation](verifiable_presentation.md)