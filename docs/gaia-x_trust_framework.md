# Gaia-X Trust Framework

The Gaia-X Trust Framework defines the process of going through and validating the set of automatically enforceable rules to achieve the minimum level of compatibility in terms of syntactic correctness, schema validity, cryptographic signature validation, attribute value consistency and attribute value verification.
The Trust Framework ensures that all participants in the Gaia-X Ecosystem are adhering to the policy rules agreed between the participants of the Ecosystem itself.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/context/#gaia-x-trust-framework) 2.2


