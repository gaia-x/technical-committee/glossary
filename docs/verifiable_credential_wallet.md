# Verifiable Credential Wallet

A wallet, also known as Verifiable Credential Wallet enables to store, manage, and present verifiable credentials.

## References
[Gaia-X Policy Rules Conformity Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/latest/) - 11.4

## See Also
[Verifiable Credentials](verifiable_credential.md)