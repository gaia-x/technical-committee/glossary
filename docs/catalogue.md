# Catalogue

A catalogue provides mechanisms to publish Data Product Descriptions (metadata) and support search or query of the descriptions. A catalogue may be realized as a centralized or decentralized service, but the capability can also be realized as a distributed functionality.


## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/enabling_services/#data-exchange-services) - 7.5


