# Example Standard

Example standards shall provide for possibilities how criteria may be implemented. Implementation as provided by such standards is not mandatory, and it is required to comply with any such standards. Gaia-X will provide additional notes, if significant differences are identified. Example Standards shall
especially help in evaluating conformity with Gaia-X, as Example Standards can be considered “implementation guidance”.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/Introduction_and_scope/#mapping-and-referencing-of-existing-standards) - 3.2.5
