# Usage Policy

Usage Policies, also known as Provider Policy constrains the consumer's use of a resource.

## References
[Gaia-X Architecture Document 23.10](https://docs.gaia-x.eu/technical-committee/architecture-document/23.10/) - 4.2.2

## See Also
- [Consumer](consumer.md)
- [Resource](resource.md)

