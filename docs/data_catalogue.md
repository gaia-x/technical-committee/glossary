# Data Catalogue

A Data Catalogue presents a set of available data and data products that can be queried.

## References
[Data Exchange Document 23.10](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/dewg/) - 2.2 

## See Also
- [Data](data.md)
- [Data Products](data_product.md)
