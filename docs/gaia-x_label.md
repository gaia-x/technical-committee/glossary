# Gaia-X Label

A Gaia-X Label is a machine readable, structured and signed document issued by the accredited Gaia-X Compliance services in case of a valid verification and validation of the criteria for a specific assessment scheme.

## References
[Gaia-X Compliance Document 24.11](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.11/annex_label_format/#gaia-x-label-format)- 8



