# GXDCH (Gaia-X Digital Clearing House)

The Gaia-X Digital Clearing House operationalizes the Gaia-X mission. A GXDCH makes the various mechanisms and concepts applicable in practice as a ready-to-use service set. 
The GXDCH contains both mandatory and optional components.
All the mandatory components of the GXDCH are open-source software. The development and architecture of the GXDCH is under the governance of the Gaia-X Association.


## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/annex/#annex-a-gaia-x-digital-clearing-house-gxdch) - 11.1

## See Also
[Gaia-X](gaia-x.md)