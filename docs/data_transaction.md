# Data Transaction 

Result of an agreement between a data provider and a data user with the purpose of exchanging, accessing
and using data, in return for monetary or non-monetary compensation.

Note 1 to entry: “Data exchange“ and “data access” terms are used in order to describe different mechanisms, like 
actual transfer of data or situations where data does not move but where access is provided to different 
stakeholders.

Note 2 to entry: Data transactions do not necessarily imply a commercial relationship.

Note 3 to entry: Each data transaction is unique and must be treated independently of other data transactions.

## References
[CEN/WS TDT Part 1](https://www.cencenelec.eu/media/CEN-CENELEC/News/Workshops/2024/2024-01-16%20-%20Data%20Transactions/cwa-draft-part1-0-8_clean.pdf)

## See Also
[Data Provider](data_provider.md)
