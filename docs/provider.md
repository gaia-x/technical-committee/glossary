# Provider

A Provider operates resources in the Gaia-X ecosystem and offers them as services through Gaia-X service offering credentials.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#provider) - 3.2.2.1

## See Also
- [Resources](resource.md)
- [Gaia-X Ecosystem](gaia-x_ecosystem.md)
- [Gaia-X Credentail](gaia-x_credential.md)

