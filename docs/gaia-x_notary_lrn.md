# Gaia-X Notary - LRN (Legal Registration Number)

It is one of the mandatory software components that are being operated only by the Gaia-X Digital Clearing House (GXDCH).
The Gaia-X Notary-LRN:
- Takes as input a LegalRegistrationNumber VC from the user;
- Verifies that the VC contains at least one identification number as requested by the Gaia-X rules, and checks that the number is valid;
- Returns a Gaia-X VC with the proof that the number has been verified.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_services/#gaia-x-notary-lrn-legal-registration-number)- 6.3

## See Also
[Gaia-X Digital Clearing Houses](GXDCH.md)