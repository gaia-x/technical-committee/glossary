# Claim

A claim is a statement about a subject. A subject is a thing about which claims can be made. Claims are expressed using subject-property-value relationships.

## References
[W3C, Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/#claims)
