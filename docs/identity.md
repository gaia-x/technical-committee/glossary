# Identity

An Identity is composed of a unique identifier and an attribute or set of attributes that uniquely describe an entity within a given context.
Gaia-X uses existing Identities and does not maintain them directly. 

## References
[Gaia-X Architecture Document 24.4](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#identities) 3.3.3.1

## See Also
[Identifier](identifier.md)