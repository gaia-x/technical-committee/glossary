# Acronyms

The following table contains the list of the acronyms used within Gaia-X.

| Acronym      | Meaning |
| ----------- | ----------- | 
| AD | Architecture Document |
| BoD | Board of Directors |
| CAB | Conformity Assessment Body |
| CD | Compliance Document |
| CSP | Cloud Service Provider |
| DEX | Data Exchange (referred to the respective WG or deliverable ) |
| DSBA | Data Spaces Business Alliance |
| DSBC | Data and Services Business Committee |
| DSSC | Data Spaces Support Centre |
| EEA | European Economic Area |
| EU Cloud CoC | European Cloud Code of Conduct |
| EFTA | European Free Trade Association | 
| EUCS | European Cloud Scheme |
| FAIR | Findable, Accessible, Interoperable and Reusable |
| GA | General Assembly |
| GAB | Governmental Advisory Board |
| GDPR | General Data Protection Regulation |
| GXDCH | Gaia-X Digital Clearing House |
| ICAM | Identity, Credentials and Access Management (referred to the respective WG or deliverable ) |
| LHP | Lighthouse Project | 
| ODRL | Open Digital Rights Language |
| OGA | Ordinary General Assembly | 
| PRC | Policy Rules Committee |
| TA | Trust Anchor |
| TC | Technical Committee |
| TCK | Technical Compatibility Kit | 
| TISAX | Trusted Information Security Assessment Exchange |
| TSP | Trust Service Provider | `
| VC | Verifiable Credential |
| W3C | Worldwide Web Consortium |
| WG | Working Group |  
