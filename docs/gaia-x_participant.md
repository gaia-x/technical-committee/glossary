# Gaia-X Participant

The Gaia-X Participant is an entity, as defined in ISO/IEC 24760-1, which is onboarded and has a Gaia-X Participant Credential.

Note: A Participant is not necessarily a member of the Gaia-X Association.

## References
[Gaia-X Architecture Document 24.04](https://docs.gaia-x.eu/technical-committee/architecture-document/24.04/gx_conceptual_model/#participant)- 3.2.2

## See Also
- [Entity](entity.md)
- [Gaia-X Member](gaia-x_member.md)